def print_colored(value, color=None):
    if color:
        print(color + value + color)
    else:
        print(value)
